/*This file is part of tilefish

    Copyright (C) 2019 Asko Ropponen

    tilefish is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    tilefish is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with tilefish.  If not, see <https://www.gnu.org/licenses/>.*/
import QtQuick 2.0
import QtGraphicalEffects 1.0
import Sailfish.Silica 1.0

import "../../js/imageViewManager.js" as ImageViewManager

import "../model"

Item {
    property string imageURL: image.source
    id: imageContainer
    objectName: "ads"
    width: parent.width
    height: parent.height
    clip: true
    // when no image loaded
    BorderImage {
        id: bImage

        visible: image.hasImage ? false : true

        source: "qrc:///ui/defaultBorder"
        x: parent.width / 100
        y: parent.width / 100
        width: parent.width - parent.width / 50
        height: parent.height - parent.width / 50

        horizontalTileMode: BorderImage.Round
        verticalTileMode: BorderImage.Round

        border.left: 85
        border.top: 85
        border.right: 85
        border.bottom: 85
        // colorize the border image
        ColorOverlay {
            anchors.fill: parent
            source: parent
            color: marea.pressed ? Theme.highlightColor : Theme.primaryColor
        }
        Label {
            anchors.fill: parent
            anchors.margins: Theme.horizontalPageMargin
            wrapMode: Text.WordWrap
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            //: Shown in image pane when no image loaded.
            //% "No Image Loaded. Tap to Load Image."
            text: qsTrId("tilefish-no-image-loaded")
            color: marea.pressed ? Theme.highlightColor : Theme.primaryColor
        }
    }
    Image {
        property bool hasImage: false

        property real origX: width / 2
        property real origY: height / 2

        // when created, check if there is an existing source
        // imageContainer.imageURL is set from outside
        Component.onCompleted: {
            if (imageContainer.imageURL !== "") {
                image.source = imageContainer.imageURL
            }
        }

        id: image
        visible: true

        width: parent.width
        height: parent.height

        fillMode: Image.PreserveAspectFit
        asynchronous: true
        transform: [
            // scaler used for mirroring. zooming uses image.scale
            Scale { id: imageScaler; origin.x: image.origX; origin.y: image.origY; xScale: 1; yScale: 1},
            Rotation { id: imageRotator; origin.x: image.origX; origin.y: image.origY; angle: 0}
        ]
    }
    PinchArea {
        enabled: image.hasImage ? true : false
        anchors.fill: parent
        pinch.target: image
        pinch.minimumScale: 0.25
        pinch.maximumScale: 5
        MouseArea {
            id: marea
            anchors.fill: parent
            onClicked: ImageViewManager.selectAction(image, imageViewBtns, imageViewTimer)
            // if no image loaded disable drag
            drag.target: image.hasImage ? image : undefined
            // you can only drag the image half a screen away. Independed of zoom level
            drag.minimumX: - image.width * (image.scale / 2)
            drag.maximumX: image.width * (image.scale / 2)
            drag.minimumY: - image.height * (image.scale / 2)
            drag.maximumY: image.height * (image.scale / 2)
        }
    }
    Flow { // Flow to allow all buttons to be seen regardless of layout
        id: imageViewBtns
        visible: false
        width: parent.width
        height: parent.height

        IconButton {
            id: buttonRotL
            icon.source: "image://theme/icon-m-rotate-left?"
            onClicked: ImageViewManager.rotateImage(image, imageRotator, "left")
        }
        IconButton {
            id: buttonRotR
            icon.source: "image://theme/icon-m-rotate-right?"
            onClicked: ImageViewManager.rotateImage(image, imageRotator, "right")
        }
        IconButton {
            id: buttonFlipH
            icon.source: "image://theme/icon-m-flip?"
            onClicked: ImageViewManager.flipImage(imageScaler, "h")
        }
        IconButton {
            id: buttonFlipV
            icon.source: "image://theme/icon-m-flip?"
            icon.rotation: 90
            onClicked: ImageViewManager.flipImage(imageScaler, "v")
        }
        IconButton {
            id: buttonResetScale
            icon.source: "image://theme/icon-m-scale?"
            onClicked: function() {
                image.scale = 1
                image.x = 0
                image.y = 0
            }
        }
        IconButton {
            id: buttonClear
            icon.source: "image://theme/icon-m-clear?"
            //: Remorse popup text when removing image from view
            //% "Clearing view"
            property string remorseText: qsTrId("tilefish-remorse-clear")
            onClicked: remorse.execute(remorseText, function() {
                ImageViewManager.clearImageView(image)
                imageViewBtns.visible = false
            })
        }
    }
    RemorsePopup { id: remorse }
    Timer {
        id: imageViewTimer
        interval: 5000
        running: false
        onTriggered: imageViewBtns.visible = false
    }

}
