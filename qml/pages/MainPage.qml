/*This file is part of tilefish

    Copyright (C) 2019 Asko Ropponen

    tilefish is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    tilefish is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with tilefish.  If not, see <https://www.gnu.org/licenses/>.*/
import QtQuick 2.0
import Sailfish.Silica 1.0

import "../model"
import "../components"

import "../../js/pageManager.js" as PageManager

Page {
    id: page

    property real halfwidth: page.width / 2
    property real halfheight: page.height / 2

    property real realheight: page.height - header.height
    property real halfrealheight: realheight / 2

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.Portrait

    SilicaFlickable {
        id: contentContainer
        width: page.width
        height: page.height
        anchors.top: page.top
        anchors.bottom: page.bottom

        PageHeader {
            id: header
            title: ""
            rightMargin: Theme.iconSizeLarge
            IconButton {
                id: buttonPrev
                enabled: {
                    if (viewPageContainer.model.count <= 1) {
                        false
                    }
                    else {
                        true
                    }
                }
                anchors.verticalCenter: header.verticalCenter
                anchors.left: header.left
                icon.source: "image://theme/icon-m-left?"
                onClicked: {
                    if (viewPageContainer.model.count !== 0) {
                        PageManager.prevPage()
                    }
                }
            }
            IconButton {
                id: buttonNext
                enabled: {
                    if (viewPageContainer.model.count <= 1) {
                        false
                    }
                    else {
                        true
                    }
                }
                anchors.verticalCenter: header.verticalCenter
                anchors.right: header.right
                icon.source: "image://theme/icon-m-right?"
                onClicked: {
                    if (viewPageContainer.model.count !== 0) {
                        PageManager.nextPage()
                    }
                }
            }
            IconButton {
                id: buttonRemovePage
                enabled: {
                    if (viewPageContainer.model.count === 0) {
                        false
                    }
                    else {
                        true
                    }
                }
                icon.source: "image://theme/icon-m-remove?"
                anchors.verticalCenter: header.verticalCenter
                anchors.right: header.horizontalCenter
                //: Remorse popup text when removing a page
                //% "Removing page"
                property string remorseText: qsTrId("tilefish-page-remove")
                onClicked: {
                    if (viewPageContainer.model.count !== 0) {
                        remorse.execute(remorseText, function() {  PageManager.removeCurrentPage() })
                    }
                }
            }
            IconButton {
                id: buttonAddPage
                icon.source: "image://theme/icon-m-add?"
                anchors.verticalCenter: header.verticalCenter
                anchors.left: header.horizontalCenter
                onClicked: {
                    var addPageDialog = pageStack.push(Qt.resolvedUrl("AddPage.qml"))
                    addPageDialog.accepted.connect(function() {
                        PageManager.addPage(contentContainer.pageNameText, addPageDialog.items, addPageDialog.columns, addPageDialog.rows)
                    })
                }
            }
            RemorsePopup { id: remorse }
        }

        // PullDownMenu and PushUpMenu must be declared in SilicaFlickable, SilicaListView or SilicaGridView
        /*PullDownMenu {
            MenuItem {
                //: Add Page -text.
                //% "Add Page..."
                text: qsTrId("tilefish-add-page")
                onClicked: {
                    var addPageDialog = pageStack.push(Qt.resolvedUrl("AddPage.qml"))
                    addPageDialog.accepted.connect(function() {
                        PageManager.addPage(contentContainer.pageNameText, addPageDialog.items, addPageDialog.columns, addPageDialog.rows)
                    })
                }
            }
        }*/
        SilicaListView {
            id: viewPageContainer
            // expose this to use in children:
            // we need to save image amount in model
            property SilicaListView viewPageContainer: viewPageContainer
            anchors.top: header.bottom

            width: parent.width
            height: realheight

            model: ViewPageListModel {}

            clip: true
            interactive: false

            snapMode: ListView.SnapOneItem
            orientation: ListView.HorizontalFlick

            highlightRangeMode: ListView.StrictlyEnforceRange
            // HACK:
            // instead of storing the image info in the model
            // (which would require storing zoom levels etc.)
            // set cache buffer really high so the images wont unload.
            //
            // on 1080 wide screen:
            // 99999999 / 1080 = 92592,59166666666666666667 pages should be possible
            // who knows if this would mess performance up
            // if that many images would be actually loaded
            cacheBuffer: 99999999

            currentIndex: this.model.currentIndex
            Binding {
                target: viewPageContainer.model
                property: "currentIndex"
                value: viewPageContainer.currentIndex
            }
            delegate: Grid {
                id: pageId
                width: page.width
                height: realheight

                columns: pageColumns
                rows: pageRows

                clip: true

                Component.onCompleted: PageManager.addImageTile(this, pageItems, pageColumns, pageRows)
            } // Grid
            // label when no pages exists
            Column {
                width: parent.width
                anchors.verticalCenter: parent.verticalCenter
                anchors.margins: Theme.horizontalPageMargin
                spacing: Theme.iconSizeSmall
                visible: {
                    if (viewPageContainer.model.count === 0) {
                       true
                    }
                    else {
                        false
                    }}
                Label {
                    width: parent.width
                    font.pixelSize: Theme.fontSizeExtraLarge
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    //: When no pages are added
                    //% "No image pages, try adding one by pressing the + button above."
                    text: qsTrId("tilefish-no-pages")
                }
                Button {
                    anchors.horizontalCenter: parent.horizontalCenter
                    //: Go to About page -button
                    //% "About this software..."
                    text: qsTrId("tilefish-goto-about")
                    onClicked: pageStack.push("AboutPage.qml")
                }
            }
        } // SilicaListView
    }
}
