/*This file is part of tilefish

    Copyright (C) 2019 Asko Ropponen

    tilefish is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    tilefish is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with tilefish.  If not, see <https://www.gnu.org/licenses/>.*/

// tap on the image
function selectAction(imageView, controls, timer) {
    if (!imageView.hasImage) {
        openFilePicker(imageView)
    }
    else {
        toggleControls(controls, timer)
    }
}
// (un)loading
function openFilePicker(imageView) {
    var selectImageDialog = pageStack.push(Qt.resolvedUrl("../qml/pages/SelectFilePage.qml"))
    selectImageDialog.onSelectedContentPropertiesChanged.connect(function() {
        setSource(imageView, selectImageDialog.fpath)
        imageView.hasImage = true
        updateModel(1)
    })
}
function clearImageView(imageView) {
    imageView.hasImage = false
    setSource(imageView, "")
    updateModel(-1)
}

function setSource(imageView, source) {
    imageView.source = source
}
function updateModel(amount) {
    var oldCount = viewPageContainer.model.get(viewPageContainer.model.currentIndex).pageImages
    var newCount
    if (oldCount === 0) {
        if (amount < 0) {
            newCount = 0
        }
        else {
            newCount = oldCount + amount
        }
    }
    else {
        newCount = oldCount + amount
    }
    viewPageContainer.model.set(viewPageContainer.model.currentIndex,{pageImages: newCount})
    // update Cover text
    app.currentPageImages = app.imagesText + ": " + newCount
}

// showcontrols
function toggleControls(controls, timer) {
    if (!controls.visible) {
        controls.visible = true
        timer.restart()
    }
    else {
        controls.visible = false
    }
}

// image manipulation
function rotateImage(image, imageRotator, dir) {
    var rot = image.rotation //imageRotator.angle
    var oldW = image.width
    var oldH = image.height
    if (dir === "right") {
        if (rot === 270) {
            rot = 0
        }
        else {
            rot += 90
        }
    }
    else if (dir === "left") {
        if (rot === 0) {
            rot = 270
        }
        else {
            rot -= 90
        }
    }
    //imageRotator.angle = rot
    image.rotation = rot
    image.fillMode = Image.PreserveAspectFit
}
function flipImage(imageScaler, dir) {
    if (dir === "h") {
        imageScaler.xScale = -imageScaler.xScale
    }
    else if (dir === "v") {
        imageScaler.yScale = -imageScaler.yScale
    }
}
