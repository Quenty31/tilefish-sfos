/*This file is part of tilefish

    Copyright (C) 2019 Asko Ropponen

    tilefish is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    tilefish is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with tilefish.  If not, see <https://www.gnu.org/licenses/>.*/

#include <QtQuick>
#include <QObject>
#include "localehelper.h"

#include <sailfishapp.h>

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    // SailfishApp::main() will display "qml/tilefish.qml", if you need more
    // control over initialization, you can use:
    //
    //   - SailfishApp::application(int, char *[]) to get the QGuiApplication *
    //   - SailfishApp::createView() to get a new QQuickView * instance
    //   - SailfishApp::pathTo(QString) to get a QUrl to a resource file
    //   - SailfishApp::pathToMainQml() to get a QUrl to the main QML file
    //
    // To display the view, call "show()" (will show fullscreen on device).

    auto app = SailfishApp::application(argc, argv);

    {
        // translator
        auto *translator = new QTranslator(app);
        // find from appfolder/translations
        auto path = SailfishApp::pathTo(QStringLiteral("translations")).toLocalFile();
        // get app name "tilefish"
        auto name = app->applicationName();
        // if no translation by name
        // filename + prefix + ui language name + suffix
        // eq: tilefish-LOCALE(.qm)
        if (!translator->load(QLocale::system(), name, "-", path))
        {
            // Load default translations if not
            // eq. tilefish
            translator->load(name, path);
            app->installTranslator(translator);
        }
        else
        {
            translator->deleteLater();
        }
    }
    // this would allow locale helper in qml files
    qmlRegisterType<LocaleHelper>("harbour.okxa", 1, 0, "LocaleHelper");
    return SailfishApp::main(argc, argv);
}
