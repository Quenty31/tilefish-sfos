/*This file is part of tilefish

    Copyright (C) 2019 Asko Ropponen

    tilefish is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    tilefish is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with tilefish.  If not, see <https://www.gnu.org/licenses/>.*/
#ifndef LOCALEHELPER_H
#define LOCALEHELPER_H

#include <QObject>
#include <QLocale>
/*!
 * \brief The LocaleHelper class is used to expose QLocale properties to QML.
 */
class LocaleHelper : public QObject
{
    Q_OBJECT
public:
    explicit LocaleHelper (QObject* parent = 0);
    Q_INVOKABLE QString getLanguageName(QString languageCode);
signals:

public slots:
};

#endif // LOCALEHELPER_H
